import java.util.ArrayList;

public class Customer implements IDisplay {
	   int Customerid;
		String firstname;
		String lastname;
		String fullname; 
		String email;
		double totalamounttopay;
		ArrayList <Bill> Billlist = new ArrayList<Bill>();
		//constructor
		public  Customer (int Customerid,String firstname,String lastname,String fullname,
		String email,double totalamounttopay, ArrayList<Bill> billlist)
		{
			this.Customerid=Customerid;
			this.firstname=firstname;
			this.lastname=lastname;
			this.fullname=fullname;
			this.email=email;
			this.totalamounttopay=totalamounttopay;
			this.Billlist = billlist;
		}
		public ArrayList<Bill> getBilllist() {
			return Billlist;
		}
		public void setBilllist(ArrayList<Bill> billlist) {
			Billlist = billlist;
		}
		//getter setter 
		public int getCustomerid() {
			return Customerid;
		};
		public void setCustomerid(int customerid) {
			Customerid = customerid;
		}
		public String getFirstname() {
			return firstname;
		}
		public void setFirstname(String firstname) {
			this.firstname = firstname;
		}
		public String getLastname() {
			return lastname;
		}
		public void setLastname(String lastname) {
			this.lastname = lastname;
		}
		public String getFullname() {
			return fullname;
		}
		public void setFullname(String fullname) {
			this.fullname = fullname;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		
		public double getTotalamounttopay() {
			return totalamounttopay;
		}
		public void setTotalamounttopay(double totalamounttopay) {
			this.totalamounttopay = totalamounttopay;
		}
	// methods
		@Override
		public void display() {
		
           System.out.println("Customer ID :"+Customerid);
           System.out.println("Customer Full anme :"+fullname);
           System.out.println("Customer Email Id :"+email);
           System.out.println("*****************************************");

			
		}
		
		
}
