import java.util.Date;

public class Mobile extends Bill implements IDisplay{
	
	
	String mobilemanufacturename;
	String planname;
	int mobilenumber;
	double internetGBused;
	double minuteused;
	
	// //constructor
	public   Mobile (int billid,Date billdate,String billtype,double totalbillamount,String mobilemanufacturename,String planname,int mobilenumber,
			double internetGBused,double minuteused) {
		
		super (billid,billdate,billtype,totalbillamount);
		this.mobilemanufacturename=mobilemanufacturename;
		this.planname=planname;
		this.mobilenumber=mobilenumber;
		this.internetGBused=internetGBused;
		this.minuteused=minuteused;
		
	}
	//getter setter
	public String getMobilemanufacturename() {
		return mobilemanufacturename;
	}
	public void setMobilemanufacturename(String mobilemanufacturename) {
		this.mobilemanufacturename = mobilemanufacturename;
	}
	public String getPlanname() {
		return planname;
	}
	public void setPlanname(String planname) {
		this.planname = planname;
	}
	public int getMobilenumber() {
		return mobilenumber;
	}
	public void setMobilenumber(int mobilenumber) {
		this.mobilenumber = mobilenumber;
	}
	public double getInternetGBused() {
		return internetGBused;
	}
	public void setInternetGBused(double internetGBused) {
		this.internetGBused = internetGBused;
	}
	public double getMinuteused() {
		return minuteused;
	}
	public void setMinuteused(double minuteused) {
		this.minuteused = minuteused;
	}
	
   public void display()
   {
		System.out.println("Bill id "+ getBillid());
		System.out.println("Bill date "+getBilldate());
		System.out.println("Bill type"+getBilltype());
		System.out.println("Bill amount "+getTotalbillamount());
		System.out.println("Manufacture name "+ getMobilemanufacturename());
		System.out.println("plan name"+getPlanname());
		System.out.println("Mobile no "+getMobilenumber());
		System.out.println("Internet use"+getInternetGBused());
		System.out.println("Minute usage"+getMinuteused());
		
   }
}
