import java.util.Date;

public class Hydro extends Bill implements IDisplay {
	
	String agencyname;
	double unitconsumed;
	//constructor
	
	public  Hydro (int billid, Date billdate,String billtype,double totalbillamount,String agencyname, double unitconsumed) {
		super (billid,billdate,billtype,totalbillamount);
		this.agencyname =agencyname;
		this.unitconsumed=unitconsumed;
	}
	//getter setter 
	public String getAgencyname() {
		return agencyname;
	}
	public void setAgencyname(String agencyname) {
		this.agencyname = agencyname;
	}
	public double getUnitconsumed() {
		return unitconsumed;
	}
	public void setUnitconsumed(double unitconsumed) {
		this.unitconsumed = unitconsumed;
	}
	
	@Override
	public void display()
	{
		System.out.println("Bill id"+getBillid());
		System.out.println("Bill date"+getBilldate());
		System.out.println("Bill type"+getBilltype());
		System.out.println("Bill amount"+getTotalbillamount());
		System.out.println("Agency name"+getAgencyname());
		System.out.println("Unit consumed"+ getUnitconsumed());
	}
	
}
