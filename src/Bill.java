import java.util.Date;

public class Bill implements IDisplay,Comparable<Bill> {

	
		// TODO Auto-generated method stub
int billid;
Date  billdate;
String billtype;
double totalbillamount;
//constructor

public Bill (int billid,Date bill_date,String billtype,double totalbillamount){
	this.billid=billid;
	this.billdate= bill_date;
	this.billtype= billtype;
	this.totalbillamount=totalbillamount;
}
//getter setter

public int getBillid() {
	return billid;
}
public Date getBilldate() {
	return billdate;
}

public void setBilldate(Date billdate) {
	this.billdate = billdate;
}

public void setBillid(int billid) {
	this.billid = billid;
}

public String getBilltype() {
	return billtype;
}
public void setBilltype(String billtype) {
	this.billtype = billtype;
}
public Double getTotalbillamount() {
	return totalbillamount;
}
public void setTotalbillamount(Double totalbillamount) {
	this.totalbillamount = totalbillamount;
}

public void display() {
	System.out.println("the bill id is" + billid);
	System.out.println("the bill date is" + billdate);
	System.out.println("the bill type is" + billtype);
	System.out.println("the bill total amount is" + totalbillamount);
	
}
public int compareTo(Bill bill2) {
	return this.getTotalbillamount().compareTo(bill2.getTotalbillamount());
}





	}


